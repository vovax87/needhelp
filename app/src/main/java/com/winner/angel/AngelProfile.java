package com.winner.angel;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.winner.angel.tutorials.simple_multiparty.R;

public class AngelProfile extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.angel_profile);
        Button callNow = findViewById(R.id.call_now);
        callNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AngelProfile.this, AnswerACall.class);
//                intent.putExtra("userType", MainActivity.ANGEL_PROVIDORE);
                startActivity(intent);
            }
        });
    }
}
