package com.winner.angel;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.winner.angel.tutorials.simple_multiparty.R;

public class FirstScreen extends AppCompatActivity {
    private String ANGEL = "ANGEL", NEED_ANGEL = "NEED_ANGEL";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.first_screen);

        Button angelButton = (Button)findViewById(R.id.i_m_an_angel);
        Button needAngelButton = (Button)findViewById(R.id.i_need_an_angel);

        angelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callSignUp(ANGEL);
            }
        });

        needAngelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callSignUp(NEED_ANGEL);
            }
        });
    }

    private void callSignUp(String type){
        Intent intent = new Intent(this, SignUpActivity.class);
        intent.putExtra("userType", type);
        startActivity(intent);
    }
}
