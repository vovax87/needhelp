package com.winner.angel;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.winner.angel.tutorials.simple_multiparty.R;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SignUpActivity extends AppCompatActivity {
    private final FirebaseFirestore db = FirebaseFirestore.getInstance();
    private String ANGEL = "ANGEL", NEED_ANGEL = "NEED_ANGEL";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        String userType = getIntent().getExtras().getString("userType");
        Button signUpButton = (Button)findViewById(R.id.signUpButton);
        signUpButton.setOnClickListener(
            new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    saveUserDetails(userType);
                }
            });
    }

    private void saveUserDetails(String userType){
        String name = ((EditText)findViewById(R.id.userName)).getText().toString();
        String email = ((EditText)findViewById(R.id.userEmail)).getText().toString();
        Boolean languageHebrew = ((CheckBox)findViewById(R.id.languageHebrew)).isChecked();
        Boolean languageEnglish = ((CheckBox)findViewById(R.id.languageEnglish)).isChecked();

        CollectionReference users = db.collection("users");

        Map<String, Object> userData = new HashMap<>();
        userData.put("name", name);
        userData.put("email", email);
        userData.put("type", userType); //Angel Or Helper

        ArrayList<String> languages = new ArrayList<String>();
        if(languageHebrew){
            languages.add("hebrew");
        }
        if(languageEnglish){
            languages.add("english");
        }
        userData.put("languages", languages);
        Log.d("saveUser", userData.toString());
        users.document(email).set(userData);
        openActivityAfterSigningUp(userType);
    }

    private void openActivityAfterSigningUp(String userType){
        Log.d("ActivityAfterSigningUp", userType);
        if(userType.equals(ANGEL)){
            Intent intent = new Intent(this, AngelProfile.class);
            startActivity(intent);
        } else{
            Intent intent = new Intent(this, CallAnAngel.class);
            startActivity(intent);
        }

    }

//    private void setLanguages(){
//        Spinner spinner = (Spinner)getView().findViewById(R.id.languagesSpinner);
//        // Create an ArrayAdapter using the string array and a default spinner layout
//        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this.getContext(),
//                R.array.languages_array, android.R.layout.simple_spinner_item);
//        // Specify the layout to use when the list of choices appears
//        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        // Apply the adapter to the spinner
//        spinner.setAdapter(adapter);
//    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
