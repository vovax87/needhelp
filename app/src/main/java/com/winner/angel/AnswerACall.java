package com.winner.angel;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.winner.angel.tutorials.simple_multiparty.R;


public class AnswerACall extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.incoming_call);
        ImageView acceptCall = findViewById(R.id.answer_call);

        acceptCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AnswerACall.this, MainActivity.class);
                intent.putExtra("userType", MainActivity.ANGEL_PROVIDORE);
                startActivity(intent);
            }
        });
    }
}
