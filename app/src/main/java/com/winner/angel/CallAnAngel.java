package com.winner.angel;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.winner.angel.tutorials.simple_multiparty.R;

import static com.winner.angel.MainActivity.ANGEL_RECIVER;

public class CallAnAngel extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.call_angel);
        Button call_now = findViewById(R.id.call_now);
        call_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openMainActivity();
            }
        });
    }

    private void openMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("userType", ANGEL_RECIVER);
        startActivity(intent);
    }


}
